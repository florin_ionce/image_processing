// dibview.cpp : implementation of the CDibView class
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1998 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#include "stdafx.h"
#include "diblook.h"

#include "dibdoc.h"
#include "dibview.h"
#include "dibapi.h"
#include "mainfrm.h"

#include "HRTimer.h"
#include "BitmapInfoDlg.h" 
#include "DlgSelectMorphologicalOperation.h"

#include "DlgHistogram.h"
#include "DlgConvolution.h"
#include <time.h>   

#include "FiltersDlg.h"

#include "dibfft.h"
#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#define BEGIN_PROCESSING() INCEPUT_PRELUCRARI()

#define END_PROCESSING(Title) SFARSIT_PRELUCRARI(Title)

#define INCEPUT_PRELUCRARI() \
	CDibDoc* pDocSrc=GetDocument();										\
	CDocTemplate* pDocTemplate=pDocSrc->GetDocTemplate();				\
	CDibDoc* pDocDest=(CDibDoc*) pDocTemplate->CreateNewDocument();		\
	BeginWaitCursor();													\
	HDIB hBmpSrc=pDocSrc->GetHDIB();									\
	HDIB hBmpDest = (HDIB)::CopyHandle((HGLOBAL)hBmpSrc);				\
	if ( hBmpDest==0 ) {												\
		pDocTemplate->RemoveDocument(pDocDest);							\
		return;															\
	}																	\
	BYTE* lpD = (BYTE*)::GlobalLock((HGLOBAL)hBmpDest);					\
	BYTE* lpS = (BYTE*)::GlobalLock((HGLOBAL)hBmpSrc);					\
	int iColors = DIBNumColors((char *)&(((LPBITMAPINFO)lpD)->bmiHeader)); \
	RGBQUAD *bmiColorsDst = ((LPBITMAPINFO)lpD)->bmiColors;	\
	RGBQUAD *bmiColorsSrc = ((LPBITMAPINFO)lpS)->bmiColors;	\
	BYTE * lpDst = (BYTE*)::FindDIBBits((LPSTR)lpD);	\
	BYTE * lpSrc = (BYTE*)::FindDIBBits((LPSTR)lpS);	\
	int dwWidth  = ::DIBWidth((LPSTR)lpS);\
	int dwHeight = ::DIBHeight((LPSTR)lpS);\
	int w=WIDTHBYTES(dwWidth*((LPBITMAPINFOHEADER)lpS)->biBitCount);	\
	HRTimer my_timer;	\
	my_timer.StartTimer();	\

#define BEGIN_SOURCE_PROCESSING \
	CDibDoc* pDocSrc=GetDocument();										\
	BeginWaitCursor();													\
	HDIB hBmpSrc=pDocSrc->GetHDIB();									\
	BYTE* lpS = (BYTE*)::GlobalLock((HGLOBAL)hBmpSrc);					\
	int iColors = DIBNumColors((char *)&(((LPBITMAPINFO)lpS)->bmiHeader)); \
	RGBQUAD *bmiColorsSrc = ((LPBITMAPINFO)lpS)->bmiColors;	\
	BYTE * lpSrc = (BYTE*)::FindDIBBits((LPSTR)lpS);	\
	int dwWidth  = ::DIBWidth((LPSTR)lpS);\
	int dwHeight = ::DIBHeight((LPSTR)lpS);\
	int w=WIDTHBYTES(dwWidth*((LPBITMAPINFOHEADER)lpS)->biBitCount);	\
	


#define END_SOURCE_PROCESSING	\
	::GlobalUnlock((HGLOBAL)hBmpSrc);								\
    EndWaitCursor();												\
/////////////////////////////////////////////////////////////////////////////


//---------------------------------------------------------------
#define SFARSIT_PRELUCRARI(Titlu)	\
	double elapsed_time_ms = my_timer.StopTimer();	\
	CString Title;	\
	Title.Format(_TEXT("%s - Proc. time = %.2f ms"), _TEXT(Titlu), elapsed_time_ms);	\
	::GlobalUnlock((HGLOBAL)hBmpDest);								\
	::GlobalUnlock((HGLOBAL)hBmpSrc);								\
    EndWaitCursor();												\
	pDocDest->SetHDIB(hBmpDest);									\
	pDocDest->InitDIBData();										\
	pDocDest->SetTitle((LPCTSTR)Title);									\
	CFrameWnd* pFrame=pDocTemplate->CreateNewFrame(pDocDest,NULL);	\
	pDocTemplate->InitialUpdateFrame(pFrame,pDocDest);	\

/////////////////////////////////////////////////////////////////////////////
// CDibView

IMPLEMENT_DYNCREATE(CDibView, CScrollView)

BEGIN_MESSAGE_MAP(CDibView, CScrollView)
	//{{AFX_MSG_MAP(CDibView)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_MESSAGE(WM_DOREALIZE, OnDoRealize)
	ON_COMMAND(ID_PROCESSING_PARCURGERESIMPLA, OnProcessingParcurgereSimpla)
	//}}AFX_MSG_MAP

	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CScrollView::OnFilePrintPreview)
	ON_COMMAND(ID_L1_ARITHMETICOPERATION, &CDibView::OnL1Arithmeticoperation)
	ON_COMMAND(ID_L1_CYANQUARTER, &CDibView::OnL1Cyanquarter)
	ON_COMMAND(ID_L2_DISPLAYHEADER, &CDibView::OnL2Displayheader)
	ON_COMMAND(ID_L2_COLORTOGRAYSCALE, &CDibView::OnL2Colortograyscale)
	ON_COMMAND(ID_L2_8BITCOLORTOGRAY, &CDibView::OnL28bitcolortogray)
	ON_COMMAND(ID_L2_SORTLUT, &CDibView::OnL2Sortlut)
	ON_COMMAND(ID_L3_DISPLAYHISTOGRAM, &CDibView::OnL3Displayhistogram)
	ON_COMMAND(ID_L3_MULTILEVELTRESHOLDING, &CDibView::OnL3Multileveltresholding)
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_L5_LABELING, &CDibView::OnL5Labeling)
	ON_COMMAND(ID_L6_BORDERTRACING, &CDibView::OnL6Bordertracing)
	ON_COMMAND(ID_L6_DRAWFROMTEXT, &CDibView::OnL6Drawfromtext)
	ON_COMMAND(ID_L7_MORPHOLOGICOPERATION, &CDibView::OnL7Morphologicoperation)
	ON_COMMAND(ID_OTHERS_EXTRACTION, &CDibView::OnOthersExtraction)
	ON_COMMAND(ID_OTHERS_OPENING, &CDibView::OnOthersOpening)
	ON_COMMAND(ID_OTHERS_CLOSING, &CDibView::OnOthersClosing)
	ON_COMMAND(ID_OTHERS_CONTOUREXTRACTION, &CDibView::OnOthersContourextraction)
	ON_COMMAND(ID_L8_BINARISATIONWITHOPTIMALTRESHOLD, &CDibView::OnL8Binarisationwithoptimaltreshold)
	ON_COMMAND(ID_L8_STRECHSHRINKHISTOGRAM, &CDibView::OnL8Strechshrinkhistogram)
	ON_COMMAND(ID_L8_HISTOGRAMEQUALISATION, &CDibView::OnL8Histogramequalisation)
	ON_COMMAND(ID_L9_CONVOLUTION, &CDibView::OnL9Convolution)
	ON_COMMAND(ID_L9_GAUSIANCONVOLUTION, &CDibView::OnL9Gausianconvolution)
	ON_COMMAND(ID_L9_LAPLACECONVOLUTION, &CDibView::OnL9Laplaceconvolution)
	ON_COMMAND(ID_L9_HIGHPASSCONVOLUTION, &CDibView::OnL9Highpassconvolution)
	ON_COMMAND(ID_L9_DIALOGCONVOLUTION, &CDibView::OnL9Dialogconvolution)
	ON_COMMAND(ID_L9_FFT, &CDibView::OnL9Fft)
	ON_COMMAND(ID_L10_MINIMUMFILTER, &CDibView::OnL10Minimumfilter)
	ON_COMMAND(ID_L10_MAXIMUMFILTER, &CDibView::OnL10Maximumfilter)
	ON_COMMAND(ID_L10_MEDIANFILTERING, &CDibView::OnL10Medianfiltering)
	ON_COMMAND(ID_L10_GAUSSIANFILTER, &CDibView::OnL10Gaussianfilter)
	ON_COMMAND(ID_L10_GAUSSIANSEPARATEDFILTER, &CDibView::OnL10Gaussianseparatedfilter)
	ON_COMMAND(ID_L11_PREWITT, &CDibView::OnL11Prewitt)
	ON_COMMAND(ID_L11_SOBEL, &CDibView::OnL11Sobel)
	ON_COMMAND(ID_L11_PREWITTGRADIENT, &CDibView::OnL11Prewittgradient)
	ON_COMMAND(ID_L11_SOBELGARDIENT, &CDibView::OnL11Sobelgardient)
	ON_COMMAND(ID_L11_SOBELTRESHOLD, &CDibView::OnL11Sobeltreshold)
	ON_COMMAND(ID_L11_CANNYEDGEDETECTION, &CDibView::OnL11Cannyedgedetection)
	ON_COMMAND(ID_COLOCVIU_INVERSA, &CDibView::OnColocviuInversa)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDibView construction/destruction

CDibView::CDibView()
{
}

CDibView::~CDibView()
{
}

/////////////////////////////////////////////////////////////////////////////
// CDibView drawing

void CDibView::OnDraw(CDC* pDC)
{
	CDibDoc* pDoc = GetDocument();

	HDIB hDIB = pDoc->GetHDIB();
	if (hDIB != NULL)
	{
		LPSTR lpDIB = (LPSTR) ::GlobalLock((HGLOBAL) hDIB);
		int cxDIB = (int) ::DIBWidth(lpDIB);         // Size of DIB - x
		int cyDIB = (int) ::DIBHeight(lpDIB);        // Size of DIB - y
		::GlobalUnlock((HGLOBAL) hDIB);
		CRect rcDIB;
		rcDIB.top = rcDIB.left = 0;
		rcDIB.right = cxDIB;
		rcDIB.bottom = cyDIB;
		CRect rcDest;
		if (pDC->IsPrinting())   // printer DC
		{
			// get size of printer page (in pixels)
			int cxPage = pDC->GetDeviceCaps(HORZRES);
			int cyPage = pDC->GetDeviceCaps(VERTRES);
			// get printer pixels per inch
			int cxInch = pDC->GetDeviceCaps(LOGPIXELSX);
			int cyInch = pDC->GetDeviceCaps(LOGPIXELSY);

			//
			// Best Fit case -- create a rectangle which preserves
			// the DIB's aspect ratio, and fills the page horizontally.
			//
			// The formula in the "->bottom" field below calculates the Y
			// position of the printed bitmap, based on the size of the
			// bitmap, the width of the page, and the relative size of
			// a printed pixel (cyInch / cxInch).
			//
			rcDest.top = rcDest.left = 0;
			rcDest.bottom = (int)(((double)cyDIB * cxPage * cyInch)
					/ ((double)cxDIB * cxInch));
			rcDest.right = cxPage;
		}
		else   // not printer DC
		{
			rcDest = rcDIB;
		}
		::PaintDIB(pDC->m_hDC, &rcDest, pDoc->GetHDIB(),
			&rcDIB, pDoc->GetDocPalette());
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDibView printing

BOOL CDibView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

/////////////////////////////////////////////////////////////////////////////
// CDibView commands


LRESULT CDibView::OnDoRealize(WPARAM wParam, LPARAM)
{
	ASSERT(wParam != NULL);
	CDibDoc* pDoc = GetDocument();
	if (pDoc->GetHDIB() == NULL)
		return 0L;  // must be a new document

	CPalette* pPal = pDoc->GetDocPalette();
	if (pPal != NULL)
	{
		CMainFrame* pAppFrame = (CMainFrame*) AfxGetApp()->m_pMainWnd;
		ASSERT_KINDOF(CMainFrame, pAppFrame);

		CClientDC appDC(pAppFrame);
		// All views but one should be a background palette.
		// wParam contains a handle to the active view, so the SelectPalette
		// bForceBackground flag is FALSE only if wParam == m_hWnd (this view)
		CPalette* oldPalette = appDC.SelectPalette(pPal, ((HWND)wParam) != m_hWnd);

		if (oldPalette != NULL)
		{
			UINT nColorsChanged = appDC.RealizePalette();
			if (nColorsChanged > 0)
				pDoc->UpdateAllViews(NULL);
			appDC.SelectPalette(oldPalette, TRUE);
		}
		else
		{
			TRACE0("\tSelectPalette failed in CDibView::OnPaletteChanged\n");
		}
	}

	return 0L;
}

void CDibView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
	ASSERT(GetDocument() != NULL);

	SetScrollSizes(MM_TEXT, GetDocument()->GetDocSize());
}


void CDibView::OnActivateView(BOOL bActivate, CView* pActivateView,
					CView* pDeactiveView)
{
	CScrollView::OnActivateView(bActivate, pActivateView, pDeactiveView);

	if (bActivate)
	{
		ASSERT(pActivateView == this);
		OnDoRealize((WPARAM)m_hWnd, 0);   // same as SendMessage(WM_DOREALIZE);
	}
}

void CDibView::OnEditCopy()
{
	CDibDoc* pDoc = GetDocument();
	// Clean clipboard of contents, and copy the DIB.

	if (OpenClipboard())
	{
		BeginWaitCursor();
		EmptyClipboard();
		SetClipboardData (CF_DIB, CopyHandle((HANDLE) pDoc->GetHDIB()) );
		CloseClipboard();
		EndWaitCursor();
	}
}



void CDibView::OnUpdateEditCopy(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(GetDocument()->GetHDIB() != NULL);
}


void CDibView::OnEditPaste()
{
	HDIB hNewDIB = NULL;

	if (OpenClipboard())
	{
		BeginWaitCursor();

		hNewDIB = (HDIB) CopyHandle(::GetClipboardData(CF_DIB));

		CloseClipboard();

		if (hNewDIB != NULL)
		{
			CDibDoc* pDoc = GetDocument();
			pDoc->ReplaceHDIB(hNewDIB); // and free the old DIB
			pDoc->InitDIBData();    // set up new size & palette
			pDoc->SetModifiedFlag(TRUE);

			SetScrollSizes(MM_TEXT, pDoc->GetDocSize());
			OnDoRealize((WPARAM)m_hWnd,0);  // realize the new palette
			pDoc->UpdateAllViews(NULL);
		}
		EndWaitCursor();
	}
}


void CDibView::OnUpdateEditPaste(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(::IsClipboardFormatAvailable(CF_DIB));
}

void CDibView::OnProcessingParcurgereSimpla() 
{
	// TODO: Add your command handler code here
	BEGIN_PROCESSING();

	// Makes a grayscale image by equalizing the R, G, B components from the LUT
	for (int k=0;  k < iColors ; k++)
		bmiColorsDst[k].rgbRed=bmiColorsDst[k].rgbGreen=bmiColorsDst[k].rgbBlue=k;

	//  Goes through the bitmap pixels and performs their negative	
	for (int i=0;i<dwHeight;i++)
		for (int j=0;j<dwWidth;j++)
		  {	
			lpDst[i*w+j]= 255 - lpSrc[i*w+j]; //makes image negative
	  }

	END_PROCESSING("Negativ imagine");
}



void CDibView::OnL1Arithmeticoperation()
{
	BEGIN_PROCESSING();
		
	for (int i = 0; i<dwHeight; i++)
		for (int j = 0; j<dwWidth; j++)
		{
			if (lpSrc[i*w + j] > 50)
				lpDst[i*w + j] = lpSrc[i*w + j] - 50; //decrease brightness
			else
				lpDst[i*w + j] = 0;
		}

	END_PROCESSING("Arithmetic Operation")
	
}


void CDibView::OnL1Cyanquarter()
{
	BEGIN_PROCESSING();
	

	if (iColors == 256){

		bmiColorsDst[10].rgbRed = 0;
		bmiColorsDst[10].rgbGreen = 255;
		bmiColorsDst[10].rgbBlue = 255;

		for (int i = 0; i < dwHeight; i++)
			for (int j = 0; j<dwWidth; j++)
			{
				if (i > dwHeight / 2 && j < dwWidth / 2)
				{
					lpDst[i*w + j] = 10;
				}
				else
				{
					if (lpSrc[i*w + j] == 10)
						lpDst[i*w + j] = 11;
				}
			}
	}
	else{
		for (int i = 0; i < dwHeight; i++)
			for (int j = 0; 3*j<dwWidth; j++)
			{
				if (3 * i > dwHeight / 2 && 3 * j < dwWidth / 2)
				{
					lpDst[i*w + 3 * j] = 255;
					lpDst[i*w + 3 * j + 1] = 255;
					lpDst[i*w + 3 * j + 2] = 0;
				}

			}
	}
	END_PROCESSING("Cyan Quarter")
}


//============================================================lab 2================================================
void CDibView::OnL2Displayheader()
{
	// TODO: Add your command handler code here
	BEGIN_SOURCE_PROCESSING;

	CBitmapInfoDlg dlgBmpHeader;

	LPBITMAPINFO pBitmapInfoSrc = (LPBITMAPINFO)lpS;
	dlgBmpHeader.my_Width.Format(_T("Image width [pixels]: %d \nImage height [pixels]: %d\nImage size: %d kb"),
		pBitmapInfoSrc->bmiHeader.biWidth, pBitmapInfoSrc->bmiHeader.biHeight, pBitmapInfoSrc->bmiHeader.biSizeImage/1024);
	// and the other info ......
	// Stores the entries of the LUT in the CString variable m_LUT
	// (associated to the edit box for displaying the LUT)
	CString buffer;
	for (int i = 0; i<iColors; i++)
	{
		buffer.Format(_T("%3d.\t%3d\t%3d\t%3d\r\n"), i,
			bmiColorsSrc[i].rgbRed,
			bmiColorsSrc[i].rgbGreen,
			bmiColorsSrc[i].rgbBlue);
		dlgBmpHeader.m_LUT += buffer;
	}


	dlgBmpHeader.DoModal();
	END_SOURCE_PROCESSING;
}


void CDibView::OnL2Colortograyscale()
{
	BEGIN_PROCESSING();

	// TODO: Add your command handler code here
	if (iColors == 0){
		for (int i = 0; i < dwHeight; i++){
			for (int j = 0; j<dwWidth; j++)
			{

				byte avg = (lpSrc[i*w + 3 * j] + lpSrc[i*w + 3 * j + 1] + lpSrc[i*w + 3 * j + 2]) / 3;
				lpDst[i*w + 3 * j] = avg;
				lpDst[i*w + 3 * j + 1] = avg;
				lpDst[i*w + 3 * j + 2] = avg;

			}
		}
	}

		
	END_PROCESSING("grayscale");
}


void CDibView::OnL28bitcolortogray()
{
	// TODO: Add your command handler code here
	BEGIN_PROCESSING();
	for (int k = 0; k < iColors; k++){
		byte avg = (bmiColorsSrc[k].rgbRed + bmiColorsSrc[k].rgbGreen + bmiColorsSrc[k].rgbBlue) / 3;

		bmiColorsDst[k].rgbRed = bmiColorsDst[k].rgbGreen = bmiColorsDst[k].rgbBlue = avg;
	}

	END_PROCESSING("grayscale");
}


void CDibView::OnL2Sortlut()
{
	BEGIN_PROCESSING();
	for (int k = 0; k < iColors; k++){
		bmiColorsDst[k].rgbRed = bmiColorsDst[k].rgbGreen = bmiColorsDst[k].rgbBlue = k;
	}

	END_PROCESSING("sort LUT");
}

//============================================================lab 3================================================

void CDibView::OnL3Displayhistogram()
{
	BEGIN_SOURCE_PROCESSING;
	int histValues[256];
	float FDPValues[256];
	// write the code for computing the histogram and store it in the array
	//of int, histValues[256]
		// write the code for computing the PDF and store it in the array of
		//double FDPValues[256]
		// instantiate a dialog box for display and associate the histogram
		
		CDlgHistogram dlg;

		//initialize with 0
		for (int i = 0; i < 256; i++){
			histValues[i] = 0;
		}

		for (int i = 0; i < dwHeight; i++){
			for (int j = 0; j < dwWidth; j++){
				for (int k = 0; k < 256; k++){
					if (lpSrc[i*w + j] == k){
						histValues[k]++;
					}
				}
				
			}
		}
		
	memcpy(dlg.m_Histogram.values, histValues, sizeof(histValues));
		
	// display the dialog box
	dlg.DoModal();
	END_SOURCE_PROCESSING;
}


int findClosest(float nr, float* finalValues, int contor){
	float diff = 10000;
	int ret;
	for (int i = 0; i < contor; i++){
		if ((nr - finalValues[i]) < diff){
			ret = i;
		}
	}
	return ret;
}

void CDibView::OnL3Multileveltresholding()
{
	// TODO: Add your command handler code here
	//BEGIN_SOURCE_PROCESSING;
	BEGIN_PROCESSING();
	int histValues[256];
	float FDPValues[256];

	CDlgHistogram dlg;

	//initialize with 0
	for (int i = 0; i < 256; i++){
		histValues[i] = 0;
	}

	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			for (int k = 0; k < 256; k++){
				if (lpSrc[i*w + j] == k){
					histValues[k]++;
				}
			}

		}
	}

	for (int i = 0; i < 256; i++){
		FDPValues[i] = (float)histValues[i] / (dwHeight*dwWidth);
	}
	int wh = 5;
	float th = 0.003;
	float * finalValues;
	finalValues = (float *)malloc(255 * sizeof(float));
	finalValues[0] = 0;
	long int contor = 1;

	for (int k = 0 + wh; k < 255 - wh; k++){
		float v = 0;
		for (int l = k - wh; l <= k + wh; l++){
			v += FDPValues[l];
		}
		v /= 2 * wh + 1;
		finalValues[contor] = v;
		contor++;
	}


	finalValues[contor] = 255;

	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			int k = findClosest(lpSrc[i*w + j], finalValues, contor);
			lpDst[i*w + j] = finalValues[k];
		}
	}

	memcpy(dlg.m_Histogram.values, histValues, sizeof(histValues));

	// display the dialog box
	dlg.DoModal();
	//END_SOURCE_PROCESSING;
	END_PROCESSING("Tresholding");
}

//============================================================lab 4================================================


void CDibView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	BEGIN_PROCESSING();

	CPoint pos = GetScrollPosition() + point;

	int x = pos.x;
	int y = dwHeight - pos.y - 1;


	int area = 0;
	int color = lpSrc[y*w + x];

	float row = 0, col = 0;
	float colMin = 255, colMax = 0, rowMin = 255, rowMax = 0;

	float ratio;
	float sus = 0, jos1 = 0, jos2 = 0;
	float elongation = 0;

	//points
	int deltaI, deltaJ;

	float thin = 0;
	int perim = 0;
	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			if (lpSrc[i*w + j] == color){
				area++; //area
				row += i; //center of mass
				col += j; //center of mass

				//perimter
				if (lpSrc[(i + 1)*w + j] == 255 || lpSrc[(i - 1)*w + j] == 255 || lpSrc[i*w + j + 1] == 255 || lpSrc[i*w + j - 1] == 255){
					perim++;
				}

				//aspect ratio
				if (row < rowMin){
					rowMin = row;
				}

				if (row > rowMax){
					rowMax = row;
				}
				if (col < colMin){
					colMin = col;
				}

				if (col > colMax){
					colMax = col;
				}


			}
		}
	}


	row = row / area;
	col = col / area;
	//elongation
	for (int i = 0; i < dwHeight; i++){
	for (int j = 0; j < dwWidth; j++){
	if (lpSrc[i*w + j] == color){

		sus += (i - row)*(j - col);
		jos1 += (j - col)*(j - col);
		jos2 += (i - row)*(i - row);
		}
	}
}

	elongation = atan2(2 * sus, (jos1 - jos2));
	elongation = elongation / 2;

	deltaI = sin(elongation) * 100;
	deltaJ = cos(elongation) * 100;

	int i1 = row + deltaI;
	int j1 = col + deltaJ;



	//thinness
	thin = 4 * 3.14*((float)area / (float)(perim*perim));

	//ratio
	ratio = ((colMax - colMin) + 1) / ((rowMax - rowMin) + 1);

	if (x > 0 && x < dwWidth && y >0 && y < dwHeight){
		CString info;
		info.Format(_T("x=%d, y=%d, color=%d \narea=%d, [ri=%f ci= %f] \nelongation=%f, perimetrul=%d \nthin=%f, ratio=%f"),
			x, y, color	, area, row, col, elongation, perim, thin, ratio);
		AfxMessageBox(info);
	}

	int projections[700];
	for (int i = 0; i < 700; i++){
		projections[i] = 0;
	}

	//compute projections on X
	for (int i = 0; i < dwWidth; i++){
		for (int j = 0; j < dwHeight; j++){
			if (lpSrc[j*w + i] == color){
				projections[i]++;
			}
		}
	}

	//drawing stuff
	CDC dc;

	dc.CreateCompatibleDC(0);

	CBitmap ddBitmap; 

	HBITMAP hDDBitmap =
		CreateDIBitmap(::GetDC(0), &((LPBITMAPINFO)lpS)->bmiHeader, CBM_INIT, lpSrc, (LPBITMAPINFO)lpS, DIB_RGB_COLORS);

	ddBitmap.Attach(hDDBitmap);

	CBitmap* pTempBmp = dc.SelectObject(&ddBitmap);

	
	CPen pen(PS_SOLID, 1, RGB(0, 255, 0));
	CPen *pTempPen = dc.SelectObject(&pen);


	dc.MoveTo(col - 10, dwHeight - row - 1 - 10);
	dc.LineTo(col + 10, dwHeight - row -1 + 10);

	dc.MoveTo(col + 10, dwHeight - row - 1 - 10);
	dc.LineTo(col - 10, dwHeight - row - 1 + 10);

	dc.MoveTo(col + deltaJ, dwHeight - deltaI - row - 1);
	dc.LineTo(col - deltaJ, dwHeight - row + deltaI - 1);

	dc.SelectObject(pTempPen);
	dc.SelectObject(pTempBmp);
	GetDIBits(dc.m_hDC, ddBitmap, 0, dwHeight, lpDst, (LPBITMAPINFO)lpD,
		DIB_RGB_COLORS);


	//draw projection on X
	for (int i = 0; i < dwWidth; i++){
		for (int j = 0; j < projections[i]; j++){
			lpDst[j*w + i] = 0;
		}
	}

	END_PROCESSING("Lab 4");
	CScrollView::OnLButtonDblClk(nFlags, point);
}


void CDibView::OnL5Labeling()
{
	// TODO: Add your command handler code here
	BEGIN_PROCESSING();
	int obj = 0;
	int bg = 255;
	int X, N, NW, NE, W;

	//equivalent pixels
	int *label = new int[w*dwHeight];
	memset(label, 0, w*dwHeight*sizeof(int)); //instantiate labels with 0

	int labelCount = 0;

	//equivalent labels
	int label1[1000], label2[1000];
	int equivalentLabelCount = 1;

	for (int i = dwHeight - 2; i > 0; i--){
		for (int j = 1; j < dwWidth - 1; j++)
		{
			X = i*w + j;
			W = i*w + (j - 1);
			NW = (i + 1) * w + (j - 1);
			N = (i + 1)*w + j;
			NE = (i + 1)*w + (j + 1);
			if (lpSrc[X] == obj)
			{
				if (lpSrc[NW] == obj && lpSrc[NE] == obj && lpSrc[N] != obj)
				{
					label[X] = label[NW];
					label1[equivalentLabelCount] = label[NE];
					label2[equivalentLabelCount] = label[NW];
					equivalentLabelCount++;
				}
				if (lpSrc[NW] == obj && (lpSrc[N] == obj || lpSrc[NE] != obj))
				{
					label[X] = label[NW];
				}
				if (lpSrc[NW] != obj && lpSrc[W] == obj && lpSrc[N] == obj)
				{
					label[X] = label[W];
					label1[equivalentLabelCount] = label[N];
					label2[equivalentLabelCount] = label[W];
					equivalentLabelCount++;
				}
				if (lpSrc[NW] != obj && lpSrc[N] != obj && lpSrc[W] == obj && lpSrc[NE] == obj)
				{
					label[X] = label[W];
					label1[equivalentLabelCount] = label[W];
					label2[equivalentLabelCount] = label[NE];
					equivalentLabelCount++;
				}
				if (lpSrc[W] == obj && lpSrc[NW] != obj && lpSrc[N] != obj && lpSrc[NE] != obj)
				{
					label[X] = label[W];
				}
				if (lpSrc[NW] != obj && lpSrc[W] != obj && lpSrc[N] == obj)
				{
					label[X] = label[N];
				}
				if (lpSrc[NW] != obj && lpSrc[W] != obj && lpSrc[N] != obj && lpSrc[NE] == obj)
				{
					label[X] = label[NE];
				}
				if (lpSrc[NW] != obj && lpSrc[W] != obj && lpSrc[N] != obj && lpSrc[NE] != obj)
				{
					labelCount++;
					label[X] = labelCount;
				}
			}
		}
	}
	

	for (int i = 0; i < dwWidth; i++){
		for (int j = 0; j < dwHeight; j++){
			if (lpSrc[i*w + j] != bg){
				bmiColorsDst[lpSrc[i * w + j]].rgbRed = rand() % 256;
				bmiColorsDst[lpSrc[i * w + j]].rgbGreen = rand() % 256;
				bmiColorsDst[lpSrc[i * w + j]].rgbBlue = rand() % 256;
			}
		}
	}


	//haralik algorithm
	int equivalentClasses[1000];
	int equivalentMax;
	for (int i = 0; i < equivalentLabelCount; i++)
		equivalentClasses[i] = -1;
	for (int i = 1; i <= equivalentLabelCount; i++)
		equivalentClasses[i] = i;

	bool change = true;
	while (change)
	{
		change = false;
		for (int i = 1; i <= equivalentLabelCount; i++)
		{
			equivalentMax = equivalentClasses[i];
			for (int k = 0; k < equivalentLabelCount; k++)
			{
				if (label1[k] == equivalentClasses[i])
				{
					if (label2[k] > equivalentMax)
						equivalentMax = label2[k];
				}
				if (label2[k] == equivalentClasses[i])
				{
					if (label1[k] > equivalentMax)
						equivalentMax = label1[k];
				}
			}
			if (equivalentMax != equivalentClasses[i])
			{
				change = true;
				equivalentClasses[i] = equivalentMax;
			}

		}
	}



	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			if (lpSrc[i*w + j] == obj){
				lpDst[i*w + j] = equivalentClasses[label[i*w + j]];
			}

		}

	}


	delete label;

	END_PROCESSING("Labeling");
}

void CDibView::OnL6Bordertracing()
{
	BEGIN_PROCESSING();
	// TODO: Add your command handler code here
	int obj = 0;
	int bg = 255;
	int dir = 0, newDir = 0;
	int currentI = 0, currentJ = 0;
	int dirArray[1000], derivArray[1000];
	int newI = 0, newJ = 0;
	bool OK;
	int PI[1000];
	int PJ[1000];

	int pCount = 0;

	int di[4] = { 1, 0, -1, 0 };
	int	dj[4] = { 0, -1, 0, 1 };

	FILE *myFile = fopen("output_lab6.txt", "w");

	if (!myFile){
		perror("Can't open file");
	}

	for (int i = 0; i < 1000; i++){
		PI[i] = -1;
		PJ[i] = -1;
		dirArray[i] = -1;
		derivArray[i] = -1;
	}
	
	int X, N, S, E, W;
	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			X = i*w + j;
			if (lpSrc[X] == obj){
				currentI = i;
				currentJ = j;
				PI[0] = i;
				PJ[0] = j;
				dirArray[0] = 0;
				derivArray[0] = 0;
				break;
			}
		}
	}
	do{
		newDir = (dir + 3) % 4;
		newI = currentI + di[newDir];
		newJ = currentJ + dj[newDir];

		while (lpSrc[newI * w + newJ] == bg){
			newDir = (newDir + 1) % 4;
			newI = currentI + di[newDir];
			newJ = currentJ + dj[newDir];
		}
		dir = newDir;
		currentI = newI;
		currentJ = newJ;
		pCount++;
		PI[pCount] = currentI;
		PJ[pCount] = currentJ;
		dirArray[pCount] = dir;
	} while (!(PI[pCount] == PI[0] && PJ[pCount] == PJ[0]));

	
	for (int i = 0; i < iColors; i++){
		bmiColorsDst[i].rgbBlue = bmiColorsDst[i].rgbRed = bmiColorsDst[i].rgbGreen = 255;
	}
	bmiColorsDst[5].rgbBlue = bmiColorsDst[5].rgbRed = bmiColorsDst[5].rgbGreen = 0;

	//derivative calculus
	fprintf(myFile, "Derivatives: \n%d, ", derivArray[0]);
	for (int i = 1; i < pCount; i++){
		derivArray[i] = abs((dirArray[i] - dirArray[i - 1]) % 4);
		fprintf(myFile, "%d, ", derivArray[i]);
	}
	fprintf(myFile, "\n\nCode: \n");
	for (int i = 0; i < pCount; i++){
		lpDst[PI[i] * w + PJ[i]] = 5;
		fprintf(myFile, "%d, ", dirArray[i]);
	}
	fclose(myFile);
	END_PROCESSING("Border Tracing");
}


void CDibView::OnL6Drawfromtext()
{
	BEGIN_PROCESSING();

	FILE *myFile = fopen("reconstruct.txt", "r");
	int startI = 0, startJ = 0, length = 0, dir;

	int di[] = { 0, 1, 1, 1, 0, -1, -1, -1 };
	int dj[] = { 1, 1, 0, -1, -1, -1, 0, 1 };

	fscanf(myFile, "%d", &startI);
	fscanf(myFile, "%d", &startJ);
	fscanf(myFile, "%d", &length);

	lpDst[startI * w + startJ] = 5;

	for (int i = 1; i <= length; i++){
		
		fscanf(myFile, "%d", &dir);
		startI = startI + di[dir];
		startJ = startJ + dj[dir];
		lpDst[startI * w + startJ] = 5;
	}

	bmiColorsDst[5].rgbBlue = bmiColorsDst[5].rgbGreen = bmiColorsDst[5].rgbRed = 0;

	fclose(myFile);
	END_PROCESSING("Draw From Text");
}

void dilation(BYTE *lpSrc, int dwHeight, int dwWidth, int w, BYTE *lpDst, int *di, int *dj, int X, int obj){
	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			X = i * w + j;
			if (lpSrc[X] == obj){
				for (int k = 0; k < 4; k++){
					lpDst[(i + di[k]) * w + (j + dj[k])] = 0;
				}
			}
		}
	}
}

void erosion(BYTE *lpSrc, int dwHeight, int dwWidth, int w, BYTE *lpDst, int *di, int *dj, int X, int obj){
	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			X = i * w + j;
			if (lpSrc[X] == obj){
				bool ok = true;
				for (int k = 0; k < 4; k++){
					if (lpSrc[(i + di[k]) * w + (j + dj[k])] != obj)
						ok = false;
				}
				if (ok == false){
					lpDst[X] = 255;
				}
			}
		}
	}
}

void CDibView::OnL7Morphologicoperation()
{
	BEGIN_PROCESSING();
	int di[4] = { 1, 0, -1, 0 };
	int	dj[4] = { 0, -1, 0, 1 };
	int X;
	int bg = 255, obj = 0;

	bmiColorsDst[200].rgbBlue = bmiColorsDst[200].rgbGreen = bmiColorsDst[200].rgbRed = 255;

	// TODO: Add your command handler code here
	CDlgSelectMorphologicalOperation dlgSelect;
	//set the default selection to the first operation 
	dlgSelect.m_OperationType = 0;
	//show the dialog in modal mode 
	dlgSelect.DoModal();
	//obtain the selection 
	switch (dlgSelect.m_OperationType) {
	case CDlgSelectMorphologicalOperation::Dilation:
		//erosion(lpSrc, dwHeight, dwWidth, w, lpDst, di, dj, X, obj);
		dilation(lpSrc, dwHeight, dwWidth, w, lpDst, di, dj, X, obj);
		break;

	case CDlgSelectMorphologicalOperation::Erosion:
		erosion(lpSrc, dwHeight, dwWidth, w, lpDst, di, dj, X, obj);
		break;
	case CDlgSelectMorphologicalOperation::Opening:
		
		break;
	case CDlgSelectMorphologicalOperation::Closing:
		
		break;
	case CDlgSelectMorphologicalOperation::ContourExtraction:

		break;
	case CDlgSelectMorphologicalOperation::AreaFilling:
		//code for region filling 
		break;


	}

	END_PROCESSING("Morphological operations");
}


void CDibView::OnOthersExtraction()
{
	BEGIN_PROCESSING();
	int di[4] = { 1, 0, -1, 0 };
	int	dj[4] = { 0, -1, 0, 1 };
	int X;
	int bg = 255, obj = 0;

	bmiColorsDst[200].rgbBlue = bmiColorsDst[200].rgbGreen = bmiColorsDst[200].rgbRed = 255;
	// TODO: Add your command handler code here
	erosion(lpSrc, dwHeight, dwWidth, w, lpDst, di, dj, X, obj);

	END_PROCESSING("Erosion");
}


void CDibView::OnOthersOpening()
{
	BEGIN_PROCESSING();
	int di[4] = { 1, 0, -1, 0 };
	int	dj[4] = { 0, -1, 0, 1 };
	int X;
	int bg = 255, obj = 0;

	bmiColorsDst[200].rgbBlue = bmiColorsDst[200].rgbGreen = bmiColorsDst[200].rgbRed = 255;
	BYTE *lpTemp = new BYTE[w*dwHeight];
	memcpy(lpTemp, lpSrc, w*dwHeight);
	erosion(lpSrc, dwHeight, dwWidth, w, lpTemp, di, dj, X, obj);
	memcpy(lpDst, lpTemp, w*dwHeight);
	dilation(lpTemp, dwHeight, dwWidth, w, lpDst, di, dj, X, obj);

	END_PROCESSING("Opening");
}


void CDibView::OnOthersClosing()
{
	BEGIN_PROCESSING();
	int di[4] = { 1, 0, -1, 0 };
	int	dj[4] = { 0, -1, 0, 1 };
	int X;
	int bg = 255, obj = 0;

	bmiColorsDst[200].rgbBlue = bmiColorsDst[200].rgbGreen = bmiColorsDst[200].rgbRed = 255;

	unsigned char *lpTemp = new unsigned char[w*dwHeight];
	memcpy(lpTemp, lpSrc, w*dwHeight);
	dilation(lpSrc, dwHeight, dwWidth, w, lpTemp, di, dj, X, obj);
	memcpy(lpDst, lpTemp, w*dwHeight);
	erosion(lpTemp, dwHeight, dwWidth, w, lpDst, di, dj, X, obj);

	END_PROCESSING("Closing");
}


void CDibView::OnOthersContourextraction()
{
	BEGIN_PROCESSING();
	int di[4] = { 1, 0, -1, 0 };
	int	dj[4] = { 0, -1, 0, 1 };
	int X;
	int bg = 255, obj = 0;

	unsigned char *lpTemp = new unsigned char[w*dwHeight];
	unsigned char *lpTemp2 = new unsigned char[w*dwHeight];
	memcpy(lpTemp, lpSrc, w*dwHeight);
	dilation(lpSrc, dwHeight, dwWidth, w, lpTemp, di, dj, X, obj);
	memcpy(lpTemp2, lpTemp, w*dwHeight);
	erosion(lpTemp, dwHeight, dwWidth, w, lpTemp2, di, dj, X, obj);


	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (lpSrc[i*w + j] == obj && lpTemp2[i*w + j] == obj)
		{
			lpDst[i*w + j] = bg;
		}
	}


	END_PROCESSING("Contour Extraction");
}


void CDibView::OnL8Binarisationwithoptimaltreshold()
{

	BEGIN_PROCESSING();

	int histValues[256];
	float m = dwHeight*dwWidth;
	float myu = 0;
	float sigma = 0;
	int functionA[256];
	int maxA;
	int treshold;
	for (int i = 0; i < 256; i++)
	{
		histValues[i] = 0;
	}
	//histogram
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
		histValues[lpSrc[i*w + j]] ++;
	//mean
	for (int i = 0; i < 256; i++)
	{
		myu = myu + i*histValues[i];
	}
	myu = myu / m;
	//deviation
	for (int i = 0; i < 256; i++)
	{
		sigma = sigma + (i - myu)*(i - myu)*histValues[i];
	}
	sigma = sqrt(sigma / m);

	//a function from 0 to 255 and treshold calculus
	functionA[0] = m;
	maxA = functionA[0];
	treshold = 0;
	for (int i = 1; i < 256; i++)
	{
		functionA[i] = functionA[i - 1] - histValues[i - 1];
		if (i*functionA[i]>maxA)
		{
			maxA = i*functionA[i];
			treshold = i;
		}
	}

	CString info;
	info.Format(_TEXT("Average: %f\nDeviation: %f\nTreshold: %d"), myu, sigma, treshold);
	AfxMessageBox(info);

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	if (lpSrc[i*w + j]>treshold)
		lpDst[i*w + j] = 255;
	else
		lpDst[i*w + j] = 0;


	CDlgHistogram dlg;
	memcpy(dlg.m_Histogram.values, histValues, sizeof(histValues));
	dlg.DoModal();

	END_PROCESSING("Optimum binarisation");
}


void CDibView::OnL8Strechshrinkhistogram()
{
	BEGIN_PROCESSING();

	int histValues[256];
	float m = dwHeight*dwWidth;
	float myuIn = 0;
	float sigmaIn = 0;
	float myuOut;
	float sigmaOut;
	float imin, omin;
	float imax, omax;
	int deltaMyu = 50;
	int deltaSigma = 50;

	for (int i = 0; i < 256; i++)
	{
		histValues[i] = 0;
	}
	//histogram
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
		histValues[lpSrc[i*w + j]] ++;
	//mean
	for (int i = 0; i < 256; i++)
	{
		myuIn = myuIn + i*histValues[i];
	}
	myuIn = myuIn / m;
	//deviation
	for (int i = 0; i < 256; i++)
	{
		sigmaIn = sigmaIn + (i - myuIn)*(i - myuIn)*histValues[i];
	}
	sigmaIn = sqrt(sigmaIn / m);

	CString info;
	info.Format(_TEXT("Media este: %f\nDeviatia este: %f\n"), myuIn, sigmaIn);
	AfxMessageBox(info);

	imin = myuIn - sigmaIn;
	imax = myuIn + sigmaIn;

	myuOut = myuIn + deltaMyu;
	sigmaOut = sigmaIn + deltaSigma;

	omin = myuOut - sigmaOut;
	omax = myuOut + sigmaOut;

	float constanta = (omax - omin) / (imax - imin);

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (omin + (lpSrc[i*w + j] - imin)*constanta < 0)
		{
			lpDst[i*w + j] = 0;
		}
		else
		{
			if (omin + (lpSrc[i*w + j] - imin)*constanta > 255)
			{
				lpDst[i*w + j] = 255;
			}
			else
			{
				lpDst[i*w + j] = omin + (lpSrc[i*w + j] - imin)*constanta;
			}
		}
	}

	END_PROCESSING("Histogram stech/shrik");
}


void CDibView::OnL8Histogramequalisation()
{
	BEGIN_PROCESSING();

	int histValues[256];
	float histpdf[256];
	float histCpdf[256];
	float m = dwHeight*dwWidth;

	for (int i = 0; i < 256; i++)
	{
		histValues[i] = 0;
	}
	//Histogram
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
		histValues[lpSrc[i*w + j]] ++;

	//PDF function
	for (int i = 0; i < 256; i++)
	{
		histpdf[i] = histValues[i] / m;
	}
	//cumulative PDF function
	histCpdf[0] = histpdf[0];
	for (int i = 0; i < 255; i++)
	{
		histCpdf[i + 1] = histCpdf[i] + histpdf[i + 1];
	}

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		lpDst[i*w + j] = histCpdf[lpDst[i*w + j]] * 255;
	}

	END_PROCESSING("Histogram Equalisation");
}

void convolution(BYTE *lpSrc, int dwHeight, int dwWidth, int w, float *lpDst, float *nucleu, int kh, int kw)

{

	for (int i = kh / 2; i < dwHeight - kh / 2; i++)
	for (int j = kw / 2; j < dwWidth - kw / 2; j++)

	{

		float suma = 0;
		for (int ii = -kh / 2; ii <= kh / 2; ii++)
		for (int jj = -kw / 2; jj <= kw / 2; jj++)
		{
			suma = suma + lpSrc[(i + ii)*w + (j + jj)] * nucleu[(ii + kh / 2)*kw + (jj + kw / 2)];
		}
		lpDst[i*w + j] = suma;
	}
}

void convolutie2(BYTE *lpSrc, int dwHeight, int dwWidth, int w, BYTE *lpDst, float *nucleu, int nn, int s)
{

	for (int i = nn; i < dwHeight - nn; i++)
	for (int j = nn; j < dwWidth - nn; j++)
	{
		float suma = 0;
		for (int ii = -nn / 2; ii <= nn / 2; ii++)
		for (int jj = -nn / 2; jj <= nn / 2; jj++)
		{
			suma = suma + lpSrc[(i + ii)*w + (j + jj)] * nucleu[(ii + nn / 2)*nn + jj + nn / 2];
		}
		if (s != 0)
			lpDst[i*w + j] = suma / s;
		else
			lpDst[i*w + j] = suma;
	}
}

void CDibView::OnL9Convolution()
{
	BEGIN_PROCESSING();
	
	
	int s = 0;
	int nn = 5;
	float *nucleu = new float[nn*nn];
	for (int i = 0; i < nn; i++)
	for (int j = 0; j < nn; j++)
	{
		nucleu[i*nn + j] = 1;
		s += nucleu[i*nn + j];
	}
	convolutie2(lpSrc, dwHeight, dwWidth, w, lpDst, nucleu, nn, s);

	END_PROCESSING("Convolution");
}




void CDibView::OnL9Gausianconvolution()
{
	BEGIN_PROCESSING();
	int s = 0;
	int nn = 3;
	float n[3][3] = { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } };
	float *nucleu = new float[nn*nn];
	for (int i = 0; i < nn; i++)
	for (int j = 0; j < nn; j++)
	{
		nucleu[i*nn + j] = n[i][j];
		s += nucleu[i*nn + j];
	}
	convolutie2(lpSrc, dwHeight, dwWidth, w, lpDst, nucleu, nn, s);
	END_PROCESSING("Gaussian Convolution");
}


void CDibView::OnL9Laplaceconvolution()
{
	BEGIN_PROCESSING();
	int s = 0;
	int nn = 3;
	//float n[3][3] = { { 0,-1,0 }, { -1,4,-1}, { 0,-1,0} };
	float n[3][3] = { { -1, -1, -1 }, { -1, 8, -1 }, { -1, -1, -1} };
	float *nucleu = new float[nn*nn];
	for (int i = 0; i < nn; i++)
	for (int j = 0; j < nn; j++)
	{
		nucleu[i*nn + j] = n[i][j];
		if (nucleu[i*nn + j ] > 0)
		s += nucleu[i*nn + j];
	}
	convolutie2(lpSrc, dwHeight, dwWidth, w, lpDst, nucleu, nn, s);
	END_PROCESSING("Laplace");
}


void CDibView::OnL9Highpassconvolution()
{
	BEGIN_PROCESSING();
	int s = 0;
	int nn = 3;
	
	float n[3][3] = { { -1, -1, -1 }, { -1, 9, -1 }, { -1, -1, -1 } };
	float *nucleu = new float[nn*nn];
	for (int i = 0; i < nn; i++)
	for (int j = 0; j < nn; j++)
	{
		nucleu[i*nn + j] = n[i][j];
		if (nucleu[i*nn + j] > 0)
			s += nucleu[i*nn + j];
	}
	convolutie2(lpSrc, dwHeight, dwWidth, w, lpDst, nucleu, nn, s);
	END_PROCESSING("HighPass");
}


void CDibView::OnL9Dialogconvolution()
{
	BEGIN_PROCESSING();

	CDlgConvolution dlgBmpHeader;

	dlgBmpHeader.DoModal();

	int s = 0;
	int nn = 3;

	float n[3][3];
	float *nucleu = new float[nn*nn];
	

	n[0][0] = dlgBmpHeader.n00;
	n[0][1] = dlgBmpHeader.n01;
	n[0][2] = dlgBmpHeader.n02;
	n[1][0] = dlgBmpHeader.n10;
	n[1][1] = dlgBmpHeader.n11;
	n[1][2] = dlgBmpHeader.n12;
	n[2][0] = dlgBmpHeader.n20;
	n[2][1] = dlgBmpHeader.n21;
	n[2][2] = dlgBmpHeader.n22;

	for (int i = 0; i < nn; i++)
	for (int j = 0; j < nn; j++)
	{
		nucleu[i*nn + j] = n[i][j];
		if (nucleu[i*nn + j] > 0)
			s += nucleu[i*nn + j];
	}
	convolutie2(lpSrc, dwHeight, dwWidth, w, lpDst, nucleu, nn, s);

	END_PROCESSING("Dialog convolution");
}


void CDibView::OnL9Fft()
{
	BEGIN_PROCESSING();
	double*real = new double[dwWidth*dwHeight];
	double*imag = new double[dwWidth*dwHeight];
	fftimage(dwWidth,dwHeight,lpSrc, (BYTE*)0,real,imag);
	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			lpDst[i*w + j] = ((i + j) & 1) ? -lpSrc[i*w + j] : lpSrc[i*w + j];
		}
	}
	ifftimage(dwWidth,dwHeight,real,imag,lpDst, (BYTE*)0);
	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			lpDst[i*w + j] = ((i + j) & 1) ? -lpSrc[i*w + j] : lpSrc[i*w + j];
		}
	}
	END_PROCESSING("FFT");
}

int* getSortedNeighbours(BYTE* lpSrc, int i, int j, int w, int n){
	int* neighbours = new int[n * n];
	int k = 0, aux;
	for (int ii = n / 2; ii >= -n / 2; ii--)
	for (int jj = n / 2; jj >= -n / 2; jj--)
	{
		neighbours[k] = lpSrc[(i + ii)*w + (j + jj)];
		k++;
	}
	for (int ii = 0; ii < n*n - 1; ii++)
	for (int jj = ii; jj < n * n; jj++)
	{
		if (neighbours[ii]>neighbours[jj])
		{
			aux = neighbours[ii];
			neighbours[ii] = neighbours[jj];
			neighbours[jj] = aux;
		}
	}
	return neighbours;
}



void CDibView::OnL10Minimumfilter()
{
	BEGIN_PROCESSING();
	CFiltersDlg dlgBmpHeader;
	dlgBmpHeader.DoModal();

	int r = dlgBmpHeader.size;

	int* neighbours = new int[r*r];
	
	for (int i = r / 2; i < dwHeight - r / 2; i++)
	for (int j = r / 2; j < dwWidth - r / 2; j++)
	{
		neighbours = getSortedNeighbours(lpSrc, i, j, w, r);
		lpDst[i*w + j] = neighbours[0];
	}
	END_PROCESSING("Minimum Filtering");
}


void CDibView::OnL10Maximumfilter()
{
	CFiltersDlg dlgBmpHeader;
	dlgBmpHeader.DoModal();

	int r = dlgBmpHeader.size;
	int* neighbours = new int[r*r];
	BEGIN_PROCESSING();
	for (int i = r / 2; i < dwHeight - r / 2; i++)
	for (int j = r / 2; j < dwWidth - r / 2; j++)
	{
		neighbours = getSortedNeighbours(lpSrc, i, j, w, r);
		lpDst[i*w + j] = neighbours[r*r - 1];
	}

	END_PROCESSING("Maximum Filtering");
}


void CDibView::OnL10Medianfiltering()
{
	CFiltersDlg dlgBmpHeader;
	dlgBmpHeader.DoModal();

	int r = dlgBmpHeader.size;
	int media;
	int* neighbours = new int[r * r];
	BEGIN_PROCESSING();
	for (int i = r / 2; i < dwHeight - r / 2; i++)
	for (int j = r / 2; j < dwWidth - r / 2; j++)
	{
		neighbours = getSortedNeighbours(lpSrc, i, j, w, r);
		lpDst[i*w + j] = neighbours[r*r / 2];
	}
	END_PROCESSING("Median Filtering");
}

float calculGaussian(float sigma, int x, int y, int x0, int y0){

	float gauss;
	float fractie;
	float exponent;
	fractie = 1 / (2 * 3.14 *sigma*sigma);
	exponent = -((x - x0)*(x - x0) + (y - y0)*(y - y0)) / (2 * sigma*sigma);
	gauss = fractie * exp(exponent);

	return gauss;
}

void CDibView::OnL10Gaussianfilter()
{
	BEGIN_PROCESSING();
	CFiltersDlg dlgBmpHeader;
	dlgBmpHeader.DoModal();

	float sigma = dlgBmpHeader.sigmar;

	int n;
	if (6 * sigma - 6 * (int)sigma > 0.5)
	{
		n = 6 * sigma + 1;
	}
	else
	{
		n = 6 * sigma;
	}

	float* gaussian = new float[n*n];
	for (int i = 0; i < n; i++)
	for (int j = 0; j < n; j++)
	{
		gaussian[i*n + j] = calculGaussian(sigma, i, j, n / 2, n / 2);
	}

	float* lpTemp = new float[dwHeight*w];
	convolution(lpSrc, dwHeight, dwWidth, w, lpTemp, gaussian, n, n);

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		lpDst[i*w + j] = lpTemp[i*w + j];
	}
	END_PROCESSING("Gaussian Filtering");
}

float calculGaussianUniDim(float sigma, int x, int x0){

	float gaussX;
	float fractie;
	float exponent;
	fractie = 1 / (sqrt(2 * 3.14) *sigma);
	exponent = -((x - x0)*(x - x0)) / (2 * sigma*sigma);
	gaussX = fractie * exp(exponent);

	return gaussX;

}



void CDibView::OnL10Gaussianseparatedfilter()
{
	BEGIN_PROCESSING();
	CFiltersDlg dlgBmpHeader;
	dlgBmpHeader.DoModal();

	float sigma = dlgBmpHeader.sigmar;
	int n;
	if (6 * sigma - 6 * (int)sigma > 0.5){
		n = 6 * sigma + 1;
	}
	else{
		n = 6 * sigma;
	}

	float* gaussianX = new float[n];
	for (int i = 0; i < n; i++){
		gaussianX[i] = calculGaussianUniDim(sigma, i, n / 2);
	}
		

	float* gaussianY = new float[n];
	for (int i = 0; i < n; i++){
		gaussianY[i] = calculGaussianUniDim(sigma, i, n / 2);
	}
		
	float* lpTemp1 = new float[dwHeight*w];
	convolution(lpSrc, dwHeight, dwWidth, w, lpTemp1, gaussianX, n, 1);

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{

		lpDst[i*w + j] = lpTemp1[i*w + j];
	}

	float* lpTemp2 = new float[dwHeight*w];
	convolution(lpDst, dwHeight, dwWidth, w, lpTemp2, gaussianY, 1, n);

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{

		lpDst[i*w + j] = lpTemp2[i*w + j];

	}

	END_PROCESSING("Separated Gaussian");
}


void CDibView::OnL11Prewitt()
{
	BEGIN_PROCESSING();
	int dx[3][3] = { { -1.0, 0, 1 }, { -1, 0, 1 }, { -1, 0, 1 } };
	int dy[3][3] = { { 1, 1, 1 }, { 0, 0, 0 }, { -1, -1, -1 } };
	

	float *dfx = new float[3*3];
	float *dfy = new float[3*3];
	int sx = 0;
	int sy = 0;


	float *angle = new float[w * dwHeight];
	for (int i = 0; i < 3; i++)
	for (int j = 0; j < 3; j++)
	{
		dfx[i*3 + j] = dx[i][j];
		sx += dfx[i*3 + j];

		dfy[i * 3 + j] = dy[i][j];
		sy += dfy[i * 3 + j];
	}

	float *lpTempx = new float[w*dwHeight];
	memset(lpTempx, 0.0f, sizeof(float));
	convolution(lpSrc, dwHeight, dwWidth, w, lpTempx, dfx, 3, 3);

	float *lpTempy = new float[w*dwHeight];
	memset(lpTempy, 0.0f, sizeof(float));
	convolution(lpSrc, dwHeight, dwWidth, w, lpTempy, dfy, 3, 3);

	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			int gx = 0;
			int gy = 0;
			int gFinal = 0;
			gx = lpTempx[i*w + j];
			gy = lpTempy[i*w + j];

			gFinal = sqrt(double(gx*gx) + double(gx*gx));
			gFinal /= 3 * sqrt(2.0);

			if (gFinal > 255){
				lpDst[i * w + j] = 255;
			}
			else if (gFinal < 0){
				lpDst[i * w + j] = 0;
			}
			else{
				lpDst[i * w + j] = gFinal;
			}
			angle[i*w + j] = atan2(double(gy), double(gx));
		}
	}
	END_PROCESSING("Prewit");
}


void CDibView::OnL11Sobel()
{
	BEGIN_PROCESSING();
	int dx[3][3] = { { -1.0, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
	int dy[3][3] = { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };


	float *dfx = new float[3 * 3];
	float *dfy = new float[3 * 3];
	int sx = 0;
	int sy = 0;

	float *angle = new float[w * dwHeight];

	for (int i = 0; i < 3; i++)
	for (int j = 0; j < 3; j++)
	{
		dfx[i * 3 + j] = dx[i][j];
		sx += dfx[i * 3 + j];

		dfy[i * 3 + j] = dy[i][j];
		sy += dfy[i * 3 + j];
	}

	float *lpTempx = new float[w*dwHeight];
	memset(lpTempx, 0.0f, sizeof(float));
	convolution(lpSrc, dwHeight, dwWidth, w, lpTempx, dfx, 3, 3);

	float *lpTempy = new float[w*dwHeight];
	memset(lpTempy, 0.0f, sizeof(float));
	convolution(lpSrc, dwHeight, dwWidth, w, lpTempy, dfy, 3, 3);

	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			int gx = 0;
			int gy = 0;
			int gFinal = 0;
			gx = lpTempx[i*w + j];
			gy = lpTempy[i*w + j];

			gFinal = sqrt(double(gx*gx) + double(gx*gx));
			gFinal /= 3 * sqrt(2.0);

			if (gFinal > 255){
				lpDst[i * w + j] = 255;
			}
			else if (gFinal < 0){
				lpDst[i * w + j] = 0;
			}
			else{
				lpDst[i * w + j] = gFinal;
			}
			angle[i*w + j] = atan2(double(gy), double(gx));
		}
	}
	END_PROCESSING("Sobel");
}


void CDibView::OnL11Prewittgradient()
{
	BEGIN_PROCESSING();
	int dx[3][3] = { { -1.0, 0, 1 }, { -1, 0, 1 }, { -1, 0, 1 } };
	int dy[3][3] = { { 1, 1, 1 }, { 0, 0, 0 }, { -1, -1, -1 } };


	float *dfx = new float[3 * 3];
	float *dfy = new float[3 * 3];
	int sx = 0;
	int sy = 0;

	BYTE *lpTemp = new BYTE[w*dwHeight];

	for (int i = 0; i < 3; i++)
	for (int j = 0; j < 3; j++)
	{
		dfx[i * 3 + j] = dx[i][j];
		sx += dfx[i * 3 + j];

		dfy[i * 3 + j] = dy[i][j];
		sy += dfy[i * 3 + j];
	}

	float *lpTempx = new float[w*dwHeight];
	memset(lpTempx, 0.0f, sizeof(float));
	convolution(lpSrc, dwHeight, dwWidth, w, lpTempx, dfx, 3, 3);


	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			if (lpTempx[i * w + j] > 255){
				lpTemp[i * w + j] = 255;
			}
			else if (lpTempx[i * w + j] < 0){
				lpTemp[i * w + j] = 0;
			}
			else{
				lpTemp[i *w + j] = lpTempx[i * w + j];
			}
			
		}
	}

	float *lpTempy = new float[w*dwHeight];
	memset(lpTempy, 0.0f, sizeof(float));
	convolution(lpTemp, dwHeight, dwWidth, w, lpTempy, dfy, 3, 3);

	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){

			if (lpTempy[i * w + j] > 255){
				lpDst[i * w + j] = 255;
			}
			else if (lpTempy[i * w + j] < 0){
				lpDst[i * w + j] = 0;
			}
			else{
				lpDst[i *w + j] = lpTempy[i * w + j];
			}
		}
	}
	END_PROCESSING("Prewit Gardient");
}


void CDibView::OnL11Sobelgardient()
{
	BEGIN_PROCESSING();
	int dx[3][3] = { { -1.0, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
	int dy[3][3] = { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };


	float *dfx = new float[3 * 3];
	float *dfy = new float[3 * 3];
	int sx = 0;
	int sy = 0;

	BYTE *lpTemp = new BYTE[w*dwHeight];

	for (int i = 0; i < 3; i++)
	for (int j = 0; j < 3; j++)
	{
		dfx[i * 3 + j] = dx[i][j];
		sx += dfx[i * 3 + j];

		dfy[i * 3 + j] = dy[i][j];
		sy += dfy[i * 3 + j];
	}

	float *lpTempx = new float[w*dwHeight];
	memset(lpTempx, 0.0f, sizeof(float));
	convolution(lpSrc, dwHeight, dwWidth, w, lpTempx, dfx, 3, 3);


	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			if (lpTempx[i * w + j] > 255){
				lpTemp[i * w + j] = 255;
			}
			else if (lpTempx[i * w + j] < 0){
				lpTemp[i * w + j] = 0;
			}
			else{
				lpTemp[i *w + j] = lpTempx[i * w + j];
			}

		}
	}

	float *lpTempy = new float[w*dwHeight];
	memset(lpTempy, 0.0f, sizeof(float));
	convolution(lpTemp, dwHeight, dwWidth, w, lpTempy, dfy, 3, 3);

	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){

			if (lpTempy[i * w + j] > 255){
				lpDst[i * w + j] = 255;
			}
			else if (lpTempy[i * w + j] < 0){
				lpDst[i * w + j] = 0;
			}
			else{
				lpDst[i *w + j] = lpTempy[i * w + j];
			}
		}
	}
	END_PROCESSING("Sobel Gardient");
}


void CDibView::OnL11Sobeltreshold()
{
	BEGIN_PROCESSING();
	int dx[3][3] = { { -1, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
	int dy[3][3] = { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };


	float *dfx = new float[3 * 3];
	float *dfy = new float[3 * 3];
	int sx = 0;
	int sy = 0;

	float *angle = new float[w * dwHeight];

	int treshold = 50;

	for (int i = 0; i < 3; i++)
	for (int j = 0; j < 3; j++)
	{
		dfx[i * 3 + j] = dx[i][j];
		sx += dfx[i * 3 + j];

		dfy[i * 3 + j] = dy[i][j];
		sy += dfy[i * 3 + j];
	}

	float *lpTempx = new float[w*dwHeight];
	memset(lpTempx, 0.0f, sizeof(float));
	convolution(lpSrc, dwHeight, dwWidth, w, lpTempx, dfx, 3, 3);

	float *lpTempy = new float[w*dwHeight];
	memset(lpTempy, 0.0f, sizeof(float));
	convolution(lpSrc, dwHeight, dwWidth, w, lpTempy, dfy, 3, 3);

	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			int gx = 0;
			int gy = 0;
			int gFinal = 0;
			gx = lpTempx[i*w + j];
			gy = lpTempy[i*w + j];

			gFinal = sqrt(double(gx*gx) + double(gx*gx));
			gFinal /= 4 * sqrt(2.0);

			if (gFinal > treshold){
				lpDst[i * w + j] = 255;
			}
			else
			{
				lpDst[i * w + j] = 0;
			}
			angle[i*w + j] = atan2(double(gy), double(gx));
		}
	}
	END_PROCESSING("Sobel Treshold");
}


void CDibView::OnL11Cannyedgedetection()
{
	BEGIN_PROCESSING();
	float sigma = 0.8;
	BYTE *lpFilter = new BYTE[w*dwHeight];
	int n;
	if (6 * sigma - 6 * (int)sigma > 0.5){
		n = 6 * sigma + 1;
	}
	else{
		n = 6 * sigma;
	}

	float* gaussianX = new float[n];
	for (int i = 0; i < n; i++){
		gaussianX[i] = calculGaussianUniDim(sigma, i, n / 2);
	}


	float* gaussianY = new float[n];
	for (int i = 0; i < n; i++){
		gaussianY[i] = calculGaussianUniDim(sigma, i, n / 2);
	}

	float* lpTemp1 = new float[dwHeight*w];
	convolution(lpSrc, dwHeight, dwWidth, w, lpTemp1, gaussianX, n, 1);

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{

		lpFilter[i*w + j] = lpTemp1[i*w + j];
	}

	float* lpTemp2 = new float[dwHeight*w];
	convolution(lpDst, dwHeight, dwWidth, w, lpTemp2, gaussianY, 1, n);

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{

		lpFilter[i*w + j] = lpTemp2[i*w + j];

	}

	int dx[3][3] = { { -1.0, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
	int dy[3][3] = { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };


	float *dfx = new float[3 * 3];
	float *dfy = new float[3 * 3];
	int sx = 0;
	int sy = 0;

	BYTE *lpTemp = new BYTE[w*dwHeight];
	BYTE *lpSobel = new BYTE[w*dwHeight];

	for (int i = 0; i < 3; i++)
	for (int j = 0; j < 3; j++)
	{
		dfx[i * 3 + j] = dx[i][j];
		sx += dfx[i * 3 + j];

		dfy[i * 3 + j] = dy[i][j];
		sy += dfy[i * 3 + j];
	}

	float *lpTempx = new float[w*dwHeight];
	memset(lpTempx, 0.0f, sizeof(float));
	convolution(lpFilter, dwHeight, dwWidth, w, lpTempx, dfx, 3, 3);


	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			if (lpTempx[i * w + j] > 255){
				lpTemp[i * w + j] = 255;
			}
			else if (lpTempx[i * w + j] < 0){
				lpTemp[i * w + j] = 0;
			}
			else{
				lpTemp[i *w + j] = lpTempx[i * w + j];
			}

		}
	}

	float *lpTempy = new float[w*dwHeight];
	memset(lpTempy, 0.0f, sizeof(float));
	convolution(lpTemp, dwHeight, dwWidth, w, lpTempy, dfy, 3, 3);

	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){

			if (lpTempy[i * w + j] > 255){
				lpSobel[i * w + j] = 255;
			}
			else if (lpTempy[i * w + j] < 0){
				lpSobel[i * w + j] = 0;
			}
			else{
				lpSobel[i *w + j] = lpTempy[i * w + j];
			}
		}
	}
	
	END_PROCESSING("Canny");
}


void CDibView::OnColocviuInversa()
{
	BEGIN_PROCESSING();
	for (int i = 0; i < dwHeight; i++){
		for (int j = 0; j < dwWidth; j++){
			if (j < dwWidth / 2){
				lpDst[i * w + j] = lpSrc[i* w + ((dwWidth/2) - j)];
			}
			else{
				lpDst[i * w + j] = lpSrc[((dwHeight - 1) - i ) * w +  j];
				if (lpDst[i * w + j] <= 245){
					lpDst[i * w + j] += 10;
				}
				else{
					lpDst[i * w + j] = 255;
				}
				
			}
		}
	}


	END_PROCESSING("Inversa");
}
