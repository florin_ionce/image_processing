// DlgSelectMorphologicalOperation.cpp : implementation file
//

#include "stdafx.h"
#include "diblook.h"
#include "DlgSelectMorphologicalOperation.h"
#include "afxdialogex.h"


// CDlgSelectMorphologicalOperation dialog

IMPLEMENT_DYNAMIC(CDlgSelectMorphologicalOperation, CDialogEx)

CDlgSelectMorphologicalOperation::CDlgSelectMorphologicalOperation(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlgSelectMorphologicalOperation::IDD, pParent)
	, m_OperationType(0)
{

}

CDlgSelectMorphologicalOperation::~CDlgSelectMorphologicalOperation()
{
}

void CDlgSelectMorphologicalOperation::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_OPERATION_TYPE, m_OperationType);
}


BEGIN_MESSAGE_MAP(CDlgSelectMorphologicalOperation, CDialogEx)
	ON_BN_CLICKED(IDC_OPERATION_TYPE, &CDlgSelectMorphologicalOperation::OnBnClickedOperationType)
	ON_BN_CLICKED(IDC_RADIO5, &CDlgSelectMorphologicalOperation::OnBnClickedRadio6)
	ON_BN_CLICKED(IDC_RADIO2, &CDlgSelectMorphologicalOperation::OnBnClickedRadio2)
	ON_BN_CLICKED(IDC_RADIO4, &CDlgSelectMorphologicalOperation::OnBnClickedRadio4)
	ON_BN_CLICKED(IDC_RADIO3, &CDlgSelectMorphologicalOperation::OnBnClickedRadio3)
	ON_BN_CLICKED(IDC_RADIO1, &CDlgSelectMorphologicalOperation::OnBnClickedRadio1)
END_MESSAGE_MAP()


// CDlgSelectMorphologicalOperation message handlers


void CDlgSelectMorphologicalOperation::OnBnClickedRadio6()
{
	// TODO: Add your control notification handler code here
}


void CDlgSelectMorphologicalOperation::OnBnClickedRadio2()
{
	// TODO: Add your control notification handler code here
}


void CDlgSelectMorphologicalOperation::OnBnClickedRadio4()
{
	// TODO: Add your control notification handler code here
}


void CDlgSelectMorphologicalOperation::OnBnClickedRadio3()
{
	// TODO: Add your control notification handler code here
}


void CDlgSelectMorphologicalOperation::OnBnClickedOperationtype()
{
	// TODO: Add your control notification handler code here
}


void CDlgSelectMorphologicalOperation::OnBnClickedRadio1()
{
	// TODO: Add your control notification handler code here
}


void CDlgSelectMorphologicalOperation::OnBnClickedOperationType()
{
	// TODO: Add your control notification handler code here
}
