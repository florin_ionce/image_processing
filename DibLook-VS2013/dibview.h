// dibview.h : interface of the CDibView class
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1998 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

class CDibView : public CScrollView
{
protected: // create from serialization only
	CDibView();
	DECLARE_DYNCREATE(CDibView)

// Attributes
public:
	CDibDoc* GetDocument()
		{
			ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDibDoc)));
			return (CDibDoc*) m_pDocument;
		}

// Operations
public:

// Implementation
public:
	virtual ~CDibView();
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view

	virtual void OnInitialUpdate();
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView,
					CView* pDeactiveView);

	// Printing support
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);

// Generated message map functions
protected:
	//{{AFX_MSG(CDibView)
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg LRESULT OnDoRealize(WPARAM wParam, LPARAM lParam);  // user message
	afx_msg void OnProcessingParcurgereSimpla();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnL1Arithmeticoperation();
	afx_msg void OnL1Cyanquarter();
	afx_msg void OnL2Displayheader();
	afx_msg void OnL2Colortograyscale();
	afx_msg void OnL28bitcolortogray();
	afx_msg void OnL2Sortlut();
	afx_msg void OnL3Displayhistogram();
	afx_msg void OnL3Multileveltresholding();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnL5Labeling();
	afx_msg void OnL6Bordertracing();
	afx_msg void OnL6Drawfromtext();
	afx_msg void OnL7Morphologicoperation();
	afx_msg void OnOthersExtraction();
	afx_msg void OnOthersOpening();
	afx_msg void OnOthersClosing();
	afx_msg void OnOthersContourextraction();
	afx_msg void OnL8Binarisationwithoptimaltreshold();
	afx_msg void OnL8Stretchshrinkhistogram();
	afx_msg void OnL8Strechshrinkhistogram();
	afx_msg void OnL8Histogramequalisation();
	afx_msg void OnL9Convolution();
	afx_msg void OnL9Gausianconvolution();
	afx_msg void OnL9Laplaceconvolution();
	afx_msg void OnL9Highpassconvolution();
	afx_msg void OnL9Dialogconvolution();
	afx_msg void OnL9Fft();
	afx_msg void OnL10Minimumfilter();
	afx_msg void OnL10Maximumfilter();
	afx_msg void OnL10Medianfiltering();
	afx_msg void OnL10Gaussianfilter();
	afx_msg void OnL10Gaussianseparatedfilter();
	afx_msg void OnL11Prewitt();
	afx_msg void OnL11Sobel();
	afx_msg void OnL11Prewittgradient();
	afx_msg void OnL11Sobelgardient();
	afx_msg void OnL11Sobeltreshold();
	afx_msg void OnL11Cannyedgedetection();
	afx_msg void OnColocviuInversa();
};

/////////////////////////////////////////////////////////////////////////////
