#pragma once


// CFiltersDlg dialog

class CFiltersDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CFiltersDlg)

public:
	CFiltersDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFiltersDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG4 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	int size;
	float sigmar;
	afx_msg void OnEnChangeEdit2();
};
