// FiltersDlg.cpp : implementation file
//

#include "stdafx.h"
#include "diblook.h"
#include "FiltersDlg.h"
#include "afxdialogex.h"


// CFiltersDlg dialog

IMPLEMENT_DYNAMIC(CFiltersDlg, CDialogEx)

CFiltersDlg::CFiltersDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CFiltersDlg::IDD, pParent)
	, size(0)
	, sigmar(0)
{

}

CFiltersDlg::~CFiltersDlg()
{
}

void CFiltersDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, size);
	DDX_Text(pDX, IDC_EDIT2, sigmar);
}


BEGIN_MESSAGE_MAP(CFiltersDlg, CDialogEx)
	ON_EN_CHANGE(IDC_EDIT2, &CFiltersDlg::OnEnChangeEdit2)
END_MESSAGE_MAP()


// CFiltersDlg message handlers


void CFiltersDlg::OnEnChangeEdit2()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}
