//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by diblook.rc
//
#define IDR_MAINFRAME                   2
#define IDR_DIBTYPE                     3
#define IDS_DIB_TOO_BIG                 4
#define IDS_CANNOT_LOAD_DIB             5
#define IDS_CANNOT_SAVE_DIB             6
#define IDD_ABOUTBOX                    100
#define IDC_EDIT1                       102
#define m_Width                         103
#define IDD_DIALOG1                     104
#define IDC_HISTOGRAM                   104
#define IDD_HISTOGRAM                   105
#define IDC_RADIO1                      105
#define IDD_DIALOG2                     106
#define IDC_RADIO2                      106
#define IDR_MENU1                       107
#define IDC_RADIO3                      107
#define IDC_RADIO4                      108
#define IDD_DIALOG3                     108
#define IDC_RADIO5                      109
#define IDD_DIALOG4                     109
#define IDC_OPERATION_TYPE              112
#define IDC_EDIT2                       113
#define IDC_EDIT3                       114
#define IDC_EDIT4                       115
#define IDC_EDIT5                       116
#define IDC_EDIT6                       117
#define IDC_EDIT7                       118
#define IDC_EDIT8                       119
#define IDC_EDIT9                       120
#define ID_PROCESSING_DUPLICATE         32770
#define ID_PROCESSING_PARCURGERESIMPLA  32771
#define ID_PROCESSING_MULTIPLETHRESHOLDING 32772
#define ID_L1_ARITHMETICOPERATION       32773
#define ID_L1_CYANQUARTER               32774
#define ID_L2_DISPLAYHEADER             32775
#define ID_L2_COLORTOGRAYSCALE          32776
#define ID_L2_8BITCOLORTOGRAY           32777
#define ID_L2_SORTLUT                   32778
#define ID_L3_DISPLAYHISTOGRAM          32779
#define ID_L3_MULTILEVELTRESHOLDING     32780
#define ID_L5_LABELING                  32781
#define ID_L6_BORDERTRACING             32782
#define ID_L6_DRAWFROMTEXT              32783
#define ID_L7                           32784
#define ID_L7_MORPHOLOGICOPERATION      32785
#define ID_L7_OTHERS                    32786
#define ID_OTHERS_EXTRACTION            32787
#define ID_OTHERS_OPENING               32788
#define ID_OTHERS_CLOSING               32789
#define ID_OTHERS_CONTOUREXTRACTION     32790
#define ID_L8_BINARISATIONWITHOPTIMALTRESHOLD 32791
#define ID_L8_STRETCHSHRINKHISTOGRAM    32792
#define ID_Menu                         32793
#define ID_L8_STRECHSHRINKHISTOGRAM     32794
#define ID_L8_HISTOGRAMEQUALISATION     32795
#define ID_L9_CONVOLUTION               32796
#define ID_L9_GAUSIANCONVOLUTION        32797
#define ID_L9_LAPLACECONVOLUTION        32798
#define ID_L9_HIGHPASSCONVOLUTION       32799
#define ID_L9_DIALOGCONVOLUTION         32800
#define ID_L9_FFT                       32801
#define ID_L10_MINIMUMFILTER            32802
#define ID_L10_MAXIMUMFILTER            32803
#define ID_L10_MEDIANFILTERING          32804
#define ID_L10_GAUSSIANFILTER           32805
#define ID_L10_GAUSSIANSEPARATEDFILTER  32806
#define ID_L11_PREWITT                  32807
#define ID_L11_SOBEL                    32808
#define ID_L11_PREWITTGRADIENT          32809
#define ID_L11_SOBELGARDIENT            32810
#define ID_L11_SOBELTRESHOLD            32811
#define ID_L11_CANNYEDGEDETECTION       32812
#define ID_COLOCVIU_INVERSA             32813

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         32814
#define _APS_NEXT_CONTROL_VALUE         121
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
