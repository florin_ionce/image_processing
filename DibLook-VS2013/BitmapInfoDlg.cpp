// BitmapInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "diblook.h"
#include "BitmapInfoDlg.h"
#include "afxdialogex.h"


// CBitmapInfoDlg dialog

IMPLEMENT_DYNAMIC(CBitmapInfoDlg, CDialogEx)

CBitmapInfoDlg::CBitmapInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CBitmapInfoDlg::IDD, pParent)
	, m_LUT(_T(""))
	, my_Width(_T(""))
{

}

CBitmapInfoDlg::~CBitmapInfoDlg()
{
}

void CBitmapInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_LUT);
	DDX_Text(pDX, m_Width, my_Width);
}


BEGIN_MESSAGE_MAP(CBitmapInfoDlg, CDialogEx)
	ON_EN_CHANGE(IDC_EDIT1, &CBitmapInfoDlg::OnEnChangeEdit1)
END_MESSAGE_MAP()


// CBitmapInfoDlg message handlers


void CBitmapInfoDlg::OnEnChangeEdit1()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialogEx::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}
