#pragma once
#include "afxwin.h"


// CDlgConvolution dialog

class CDlgConvolution : public CDialog
{
	DECLARE_DYNAMIC(CDlgConvolution)

public:
	CDlgConvolution(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgConvolution();

// Dialog Data
	enum { IDD = IDD_DIALOG3 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	int n00;
	afx_msg void OnEnChangeEdit1();
	int n01;
	int n02;
	int n10;
	int n11;
	int n12;
	int n20;
	
	int n22;
	int n21;
};
