// DlgConvolution.cpp : implementation file
//

#include "stdafx.h"
#include "diblook.h"
#include "DlgConvolution.h"
#include "afxdialogex.h"


// CDlgConvolution dialog

IMPLEMENT_DYNAMIC(CDlgConvolution, CDialog)

CDlgConvolution::CDlgConvolution(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgConvolution::IDD, pParent)
	, n00(0)
	, n01(0)
	, n02(0)
	, n10(0)
	, n11(0)
	, n12(0)
	, n20(0)
	, n22(0)
	, n21(0)
{

}

CDlgConvolution::~CDlgConvolution()
{
}

void CDlgConvolution::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, n00);
	DDX_Text(pDX, IDC_EDIT2, n01);
	DDX_Text(pDX, IDC_EDIT3, n02);
	DDX_Text(pDX, IDC_EDIT4, n10);
	DDX_Text(pDX, IDC_EDIT5, n11);
	DDX_Text(pDX, IDC_EDIT6, n12);
	DDX_Text(pDX, IDC_EDIT7, n20);
	DDX_Text(pDX, IDC_EDIT9, n22);
	DDX_Text(pDX, IDC_EDIT8, n21);
}


BEGIN_MESSAGE_MAP(CDlgConvolution, CDialog)
	ON_EN_CHANGE(IDC_EDIT1, &CDlgConvolution::OnEnChangeEdit1)
END_MESSAGE_MAP()


// CDlgConvolution message handlers


void CDlgConvolution::OnEnChangeEdit1()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO:  Add your control notification handler code here
}
