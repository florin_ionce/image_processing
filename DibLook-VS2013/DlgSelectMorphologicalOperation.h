#pragma once
#include "afxwin.h"


// CDlgSelectMorphologicalOperation dialog

class CDlgSelectMorphologicalOperation : public CDialogEx
{
public:
	DECLARE_DYNAMIC(CDlgSelectMorphologicalOperation)
	CDlgSelectMorphologicalOperation(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgSelectMorphologicalOperation();
	enum EOperationType{
		Dilation,
		Erosion,
		Opening,
		Closing,
		ContourExtraction,
		AreaFilling,
	};
// Dialog Data
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedRadio6();
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedRadio4();
	afx_msg void OnBnClickedRadio3();
	afx_msg void OnBnClickedOperationtype();
	afx_msg void OnBnClickedRadio1();
	int m_OperationType;
	afx_msg void OnBnClickedOperationType();

};
